  Description
====================================
payflowpro_hcp helps website to implement payment process through PayFlowPro 
Hosted Checkout Pages. 


 Installation
====================================
Regular Drupal module installation.  


 Configuration
====================================
Once module installed, you can add paypal account in admin page
/admin/settings/payflowpro_hcp

Please set default account, this module will make payment through default 
module.

Unique key to be configured for each paypal account, you can insist this 
module to do payment through specific account by passing key of the account.


 API
====================================
payflowpro_hcp_invoke_payment($parameters)
- This function will invoke payment process, pass values to PayPal using 
parameter $parameters in array format.  

payflowpro_hcp_parameters()
- This function returns list of parameter names can be used to pass value to 
PayPal.

payflowpro_hcp_response_parameters()
- This function returns list of parameter names will be availoabe in PayPal 
response.

hook_payflowpro_hcp_complete($action, $response)
- This hook will be triggered after completing payment process, you can use 
this hook to do post payment activities.

 Templates
====================================
payflowpro-hcp-error-page.tpl.php
- Error message when initiating payment process can be designed in this teplate 
file, copy this template file to theme folder and override error message in this 
file.
