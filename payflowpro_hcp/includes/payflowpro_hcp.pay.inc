<?php

/**
 * @file
 * Payment process and related functions.
 */

/**
 * Initiate PayFlowPro hosted checkout page.
 */
function payflowpro_hcp_make_payment($parameters) {
  require_once 'payflowpro_hcp.admin.inc';
  if (isset($parameters['use_payflowpro']) && !$parameters['use_payflowpro']) {
    return '';
  }
  $key = (isset($parameters['payflowpro_account_key']) ? $parameters['payflowpro_account_key'] : '');

  $pay_flow_pro = payflowpro_hcp_get_account_details($key);
  // Add PayFlowPro details and generate secure token.
  if (isset($pay_flow_pro['user'])) {
    $parameters['USER'] = $pay_flow_pro['user'];
    $parameters['VENDOR'] = $pay_flow_pro['merchant'];
    $parameters['PARTNER'] = $pay_flow_pro['partner'];
    $parameters['PWD'] = $pay_flow_pro['password'];
  }
  // Secure token id.
  $timenow = date('Y-m-d H:i:s');
  $timestamp = strtotime($timenow);
  $securetokenid = drupal_substr($timestamp . '~' . $parameters['ORDERID'] . '~' . md5(uniqid(rand(), TRUE)), 0, 36);
  $parameters['SECURETOKENID'] = $securetokenid;
  $parameters['CREATESECURETOKEN'] = 'Y';
  // Request for secure token.
  $curl_url = str_replace("&amp;", "&", urldecode($pay_flow_pro['payflowpro_url']));
  $curl_param = array();
  foreach ($parameters as $key => $value) {
    $curl_param[] = "$key=$value";
  }
  $curl_params = implode('&', $curl_param);

  // Log input values.
  $log_parameters = array();
  $log_parameters = $parameters;
  unset($log_parameters['PWD']);
  unset($log_parameters['payflowpro_account_key']);
  unset($log_parameters['LOG']);
  if (isset($parameters['LOG']) && is_array($parameters['LOG'])) {
    foreach ($parameters['LOG'] as $key => $value) {
      $log_parameters[$key] = $value;
    }
  }
  $log_parameters_values = payflowpro_hcp_implode_array($log_parameters);
  watchdog('PayFlowPro', 'New payment request made with input @values', array('@values' => $log_parameters_values));
  // Payment process.
  $options = array(
    CURLOPT_RETURNTRANSFER => TRUE,
    CURLOPT_HEADER         => FALSE,
    CURLOPT_VERBOSE        => FALSE,
    CURLOPT_SSL_VERIFYHOST => 0,
    CURLOPT_SSL_VERIFYPEER => FALSE,
    CURLOPT_POST           => 1,
    CURLOPT_POSTFIELDS     => $curl_params,
  );
  $ch = curl_init($curl_url);
  curl_setopt_array($ch, $options);
  $content = curl_exec($ch);
  curl_getinfo($ch);
  curl_close($ch);
  $payment_error = '';
  // Validate secure token and redirect page to PayFlowPro.
  if ($content != '') {
    $response = array();
    foreach (explode('&', $content) as $item) {
      $key_value = explode('=', $item);
      $response[$key_value[0]] = $key_value[1];
    }
    /* validate response and redirect to PayPal page*/
    if (isset($response['RESULT']) && $response['RESULT'] == '0') {
      $parameters = array(
        'SECURETOKEN' => $response['SECURETOKEN'],
        'SECURETOKENID' => $response['SECURETOKENID'],
      );
      drupal_goto($pay_flow_pro['payflowlink_url'] . '?' . http_build_query($parameters, '', '&'));
    }
    else {
      $payment_error = "(Order Id : " . $parameters['ORDERID'] . ") Error in generating security token : $content";
      watchdog('PayFlowPro', $payment_error);
    }
  }
  else {
    $payment_error = "(Order Id : " . $parameters['ORDERID'] . ") Unable to get security token";
    watchdog('PayFlowPro', $payment_error);
  }
  if ($payment_error != '') {
    return $payment_error;
  }
  return '';
}

/**
 * On PayFlowPro hosted checkout page complete.
 */
function payflowpro_hcp_complete($action) {
  $response = $_POST;
  if ($action == 'cancel') {
    if (!isset($response['SECURETOKENID']) && isset($_GET['SECURETOKENID'])) {
      $response['SECURETOKENID'] = $_GET['SECURETOKENID'];
    }
  }
  if (!isset($response['ORDERID']) && isset($response['SECURETOKENID'])) {
    $secore_token_id = explode('~', $response['SECURETOKENID']);
    if (isset($secore_token_id[1])) {
      $response['ORDERID'] = $secore_token_id[1];
    }
  }
  if (!isset($response['ORDERID']) && !isset($response['SECURETOKENID']) && $action == 'error') {
    return theme('payflowpro_hcp_error_page', $response);
  }
  // Log output values.
  $response_value = payflowpro_hcp_implode_array($response);
  watchdog('PayFlowPro', 'Response from PayFlowPro @values', array('@values' => $response_value));
  // Avoid duplicate actions on silent_post.
  if ($action == 'success' || ($action == 'silent_post' && $response['result'] == '0')) {
    $secure_token_id = $response['SECURETOKENID'];
    $successful_transactions = variable_get('payflowpro_paid_trans', array());
    if (isset($successful_transactions[$secure_token_id])) {
      return '';
    }
    else {
      $successful_transactions[$secure_token_id] = time();
      variable_set('payflowpro_paid_trans', $successful_transactions);
      $action = 'success';
    }
  }
  module_invoke_all('payflowpro_hcp_complete', $action, $response);
}

/**
 * On PayFlowPro hosted checkout page complete.
 */
function payflowpro_hcp_implode_array($input) {
  if (!is_array($input)) {
    return '';
  }
  $output = implode(', ', array_map(
      function ($v, $k) {
        return $k . '=' . $v;
      },
      $input,
      array_keys($input)
  ));
  return $output;
}
