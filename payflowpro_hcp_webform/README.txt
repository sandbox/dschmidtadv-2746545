  Description
====================================
payflowpro_hcp_webform helps to implement PayFlowPro Hosted Checkout Page on 
webform submission. 


 Installation
====================================
Regular Drupal module installation.  


 Configuration
====================================
You can configure PayFlowPro settings in webform edit page. To make webform to 
do payment process on submission you should do following in PayFlowPro settings
1) "Do payment through PayFlowPro hosted checkout page after form submission"
- Turn on PayFlowPro Hosted Checkout page checkbox 
2) Parameters
- Configure webform fields for PayPal Input Parameters that need to be passed to 
  PayPal during payment process like "First Name", "Last Name", etc., 
3) Update Field after successful payment
- Configure webform fields for PayPal Response Parameters whose values to be 
  stored back to webform after completing payment process
4) URL to redirect after payment process
- Configure URLs of Success and Error messages


 API
====================================
hook_payflowpro_hcp_webform_prepayment(&$parameters, $form_state)
- This hook will be triggered before initiating payment process, using custom 
  module values of $parameters (PayPal input parameters) can be updated before 
  payemnt process.

hook_webform_payflowpro_hcp_complete($action, $response)
- This hook will be triggered after completing payment process, post payemnt 
  activities can be performed using this hook.

hook_payflowpro_hcp_webform_preemail(&$parameters, &$message)
- This hook will be triggered before sending webform emails, it will be 
  triggerred   in below 2 situations
  a) Immediately after submission of webform
     - Default value of parameter 'stop_email' will be true, so emails will 
     not be triggered in this event.
     - If you want to send emails immediately after webform submission then 
     change value of parameter 'stop_email' to false
          $parameters['stop_email'] = false;
  b) After successful payment
     - Emails will be triggered to users after successful payment (emails 
     that are configured in webform)

 Templates
====================================
payflowpro-hcp-webform-error-page.tpl.php
- Error message on invalid payment response can be designed in this teplate 
  file, copy this template file to theme folder and override error message 
  in this file.
