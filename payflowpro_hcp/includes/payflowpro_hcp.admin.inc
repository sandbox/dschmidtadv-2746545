<?php
/**
 * @file
 * Administration form settings and related functions.
 */

/**
 * Creating PayFlowPro account list page.
 */
function payflowpro_hcp_account_list() {
  $output = l(t('Add New PayFlowPro Account'), 'admin/settings/payflowpro_hcp/add');
  $output .= '<br /><br />' . drupal_get_form('payflowpro_hcp_list_form');
  return $output;
}

/**
 * Render account list form.
 */
function payflowpro_hcp_list_form() {
  $form = array();
  $account_header = array('Key', 'Title', 'Default', 'Enabled', '', '');
  $account_rows = array();
  $available_keys = variable_get('payflowpro_hcp_settings', array());
  $default_key = $available_keys['default'];
  if (isset($available_keys['keys'])) {
    foreach ($available_keys['keys'] as $key => $obj) {
      $current_key_item = payflowpro_hcp_form_settings_get($key);
      if (isset($current_key_item['key']) > 0) {
        $account_rows[] = array(
          array('data' => $current_key_item['key'], 'class' => 'key'),
          array('data' => $current_key_item['title'], 'class' => 'title'),
          array(
            'data' => '<input type="radio" name="default_account" value="' . $current_key_item['key'] . '" ' . ($current_key_item['key'] == $default_key ? 'checked' : '') . '  />',
            'class' => 'is_default',
          ),
          array(
            'data' => (isset($current_key_item['is_enabled']) && $current_key_item['is_enabled'] == 1 ? 'Enabled' : 'Disabled'),
            'class' => 'is_enabled',
          ),
          array(
            'data' => l(t('Edit'), 'admin/payflowpro_hcp/edit/' . $current_key_item['key']),
            'class' => 'edit',
          ),
          array(
            'data' => l(t('Delete'), 'admin/payflowpro_hcp/delete/' . $current_key_item['key']),
            'class' => 'delete',
          ),
        );
      }
    }
  }
  // Invoke hook_payflowpro_hcp_list_preprocess.
  foreach (module_implements('payflowpro_hcp_list_preprocess') as $module) {
    $function = $module . '_payflowpro_hcp_list_preprocess';
    $function($account_header, $account_rows);
  }

  $form['list'] = array(
    '#type' => 'item',
    '#value' => theme('table', $account_header, $account_rows),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  $form['#submit'][] = 'payflowpro_hcp_account_list_submit';
  return $form;
}

/**
 * On submitting list form.
 */
function payflowpro_hcp_account_list_submit($form, &$form_state) {
  $available_keys = variable_get('payflowpro_hcp_settings', array());
  $available_keys['default'] = check_plain($_POST['default_account']);
  variable_set('payflowpro_hcp_settings', $available_keys);
}

/**
 * Creating admin settings form to add PayFlowPro account details.
 */
function payflowpro_hcp_form() {
  $form = array();
  payflowpro_hcp_get_settings_fieldset($form);
  return $form;
}

/**
 * Returns form of PayFlowPro Account Settings.
 */
function payflowpro_hcp_get_settings_fieldset(&$form, $settings = array()) {
  $form['current_key'] = array(
    '#type' => 'hidden',
    '#value' => (isset($settings['key']) ? $settings['key'] : ''),
  );
  $form['payflowpro_hcp_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => (isset($settings['title']) ? $settings['title'] : ''),
    '#description' => t("Title of PayflowPro account settings"),
    '#required' => TRUE,
  );
  $form['payflowpro_hcp_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Key'),
    '#default_value' => (isset($settings['key']) ? $settings['key'] : ''),
    '#description' => t("Key of PayflowPro account settings"),
    '#required' => !isset($settings['key']),
    '#attributes' => (isset($settings['key']) ? array('readonly' => '') : array()),
  );
  $form['payflowpro_hcp_partner'] = array(
    '#type' => 'textfield',
    '#title' => t('Partner'),
    '#default_value' => (isset($settings['partner']) ? $settings['partner'] : ''),
    '#description' => t("The ID provided to you by the authorized PayPal Reseller who registered you for Payflow Pro. If you purchased your account directly from PayPal, use PayPal."),
    '#required' => TRUE,
  );
  $form['payflowpro_hcp_merchant'] = array(
    '#type' => 'textfield',
    '#title' => t('Merchant login'),
    '#default_value' => (isset($settings['merchant']) ? $settings['merchant'] : ''),
    '#description' => t("Your merchant login ID that you created when you registered for the account."),
    '#required' => TRUE,
  );
  $form['payflowpro_hcp_user'] = array(
    '#type' => 'textfield',
    '#title' => t('User'),
    '#default_value' => (isset($settings['user']) ? $settings['user'] : ''),
    '#description' => t("The ID provided to you by the authorized PayPal Reseller who registered you for Payflow Pro. If you purchased your account directly from PayPal, use PayPal."),
    '#required' => TRUE,
  );
  $form['payflowpro_hcp_password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#default_value' => '',
    '#description' => t("The password that you defined while registering for the account. <br />Leave this field blank if there is no change in password"),
    '#required' => FALSE,
  );
  $form['payflowpro_hcp_mode'] = array(
    '#type' => 'select',
    '#title' => t('Mode'),
    '#options' => array('test' => t('Test'), 'live' => t('Live')),
    '#default_value' => (isset($settings['mode']) ? $settings['mode'] : ''),
    '#description' => t("The password that you defined while registering for the account."),
    '#required' => TRUE,
  );
  $form['payflowpro_hcp_enabled'] = array(
    '#type' => 'select',
    '#title' => t('Enabled'),
    '#options' => array('1' => t('Enable'), '0' => t('Disable')),
    '#default_value' => (isset($settings['is_enabled']) ? $settings['is_enabled'] : '0'),
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['back_to_list'] = array(
    '#type' => 'markup',
    '#value' => l(t('Back To List'), 'admin/settings/payflowpro_hcp'),
  );

  $form['#submit'][] = 'payflowpro_hcp_form_submit';
  $form['#validate'][] = 'payflowpro_hcp_form_validate';
}

/**
 * PayFlowPro Account Settings form validation.
 */
function payflowpro_hcp_form_validate($form, &$form_state) {
  if (!empty($form_state['values']['payflowpro_hcp_key']) && !preg_match('/^[a-z0-9_]+$/', $form_state['values']['payflowpro_hcp_key'])) {
    form_set_error('payflowpro_hcp_key', t('Key should contain only lower case letters, numbers and underscore'));
  }
  elseif (!empty($form_state['values']['payflowpro_hcp_key']) && $form_state['values']['payflowpro_hcp_key'] != $form_state['values']['current_key']) {
    $current_settings = payflowpro_hcp_form_settings_get($form_state['values']['payflowpro_hcp_key']);
    if (isset($current_settings['key']) > 0) {
      form_set_error('payflowpro_hcp_key', $form_state['values']['payflowpro_hcp_key'] . ' already exist, please enter new key');
    }
  }
}

/**
 * Submit form data.
 */
function payflowpro_hcp_form_submit($form, &$form_state) {
  $current_key = $form_state['values']['current_key'];
  $new_key = $form_state['values']['payflowpro_hcp_key'];
  $current_settings = payflowpro_hcp_form_settings_get($current_key);

  if (!empty($form_state['values']['payflowpro_hcp_password'])) {
    $password_array = unserialize(encrypt($form_state['values']['payflowpro_hcp_password']));
    $password = $password_array['text'];
  }
  else {
    $password = (isset($current_settings['password']) ? $current_settings['password'] : '');
  }

  $new_settings = array(
    'title' => $form_state['values']['payflowpro_hcp_title'],
    'key' => $form_state['values']['payflowpro_hcp_key'],
    'partner' => $form_state['values']['payflowpro_hcp_partner'],
    'merchant' => $form_state['values']['payflowpro_hcp_merchant'],
    'user' => $form_state['values']['payflowpro_hcp_user'],
    'password' => $password,
    'mode' => $form_state['values']['payflowpro_hcp_mode'],
    'is_enabled' => $form_state['values']['payflowpro_hcp_enabled'],
  );

  payflowpro_hcp_form_settings_set($new_key, $new_settings);
  if ($current_key != $new_key) {
    payflowpro_hcp_form_settings_del($current_key);
  }

  drupal_set_message(t('Configuration saved successfully'));

  if ($current_key == '') {
    $form_state['redirect'] = 'admin/payflowpro_hcp/edit/' . $new_key;
  }
}

/**
 * Creating admin settings form to edit PayFlowPro account details.
 */
function payflowpro_hcp_edit_form() {
  $key = arg(3);
  $settings = payflowpro_hcp_form_settings_get($key);
  $form = array();
  payflowpro_hcp_get_settings_fieldset($form, $settings);
  return $form;
}

/**
 * Creating admin settings form to edit PayFlowPro account details.
 */
function payflowpro_hcp_delete_form() {
  $key = arg(3);
  $form = array();
  $form['account_key'] = array(
    '#type' => 'hidden',
    '#value' => $key,
  );
  $form['#submit'][] = 'payflowpro_hcp_delete_form_submit';
  return confirm_form($form, 'Are you sure you want to delete the PayFlowPro account settings for ' . $key . '?', 'admin/settings/payflowpro_hcp', 'You cannot recover the configuration once deleted');
}

/**
 * Submit delete form.
 */
function payflowpro_hcp_delete_form_submit($form, &$form_state) {
  $key = $form_state['values']['account_key'];
  payflowpro_hcp_form_settings_del($key);
  $form_state['redirect'] = 'admin/settings/payflowpro_hcp';
}

/**
 * Returns account settings.
 */
function payflowpro_hcp_form_settings_get($key) {
  $payflowpro_hcp_settings = variable_get('payflowpro_hcp_settings_' . $key, serialize(array()));
  return unserialize($payflowpro_hcp_settings);
}

/**
 * Update account settings.
 */
function payflowpro_hcp_form_settings_set($key, $settings) {
  variable_set('payflowpro_hcp_settings_' . $key, serialize($settings));
  $available_keys = variable_get('payflowpro_hcp_settings', array());
  $available_keys['keys'][$key] = $key;
  variable_set('payflowpro_hcp_settings', $available_keys);
}

/**
 * Delete account settings.
 */
function payflowpro_hcp_form_settings_del($key) {
  variable_del('payflowpro_hcp_settings_' . $key);
  $available_keys = variable_get('payflowpro_hcp_settings', array());
  if (isset($available_keys['keys'][$key])) {
    unset($available_keys['keys'][$key]);
  }
  variable_set('payflowpro_hcp_settings', $available_keys);
}

/**
 * Get account settings.
 */
function payflowpro_hcp_get_payflowpro_password($key = '') {
  if ($key != '') {
    $settings = payflowpro_hcp_form_settings_get($key);
    $password = $settings['password'];
    $password = decrypt(serialize(array('text' => $password)));
    return $password;
  }
  return '';
}

/**
 * To get PayFlowPro account details.
 */
function payflowpro_hcp_get_account_details($accountkey = '') {
  // If key is given.
  if (isset($accountkey)) {
    if (is_array($accountkey)) {
      foreach ($accountkey as $key) {
        $settings = payflowpro_hcp_form_settings_get($key);
        if ($settings['is_enabled'] == 1) {
          break;
        }
      }
    }
    else {
      $settings = payflowpro_hcp_form_settings_get($accountkey);
    }
  }
  if ($settings['is_enabled'] != 1) {
    $settings = array();
  }

  // If not yet rendered valid settings yet, check default settings.
  if (!isset($settings['key'])) {
    $available_keys = variable_get('payflowpro_hcp_settings', array());
    $key = $available_keys['default'];
    $settings = payflowpro_hcp_form_settings_get($key);
    if ($settings['is_enabled'] != 1) {
      $settings = array();
    }
  }
  // If no valid settings for given parameter then return empty array.
  if (!isset($settings['key'])) {
    return array();
  }
  // Render output content.
  $mode = $settings['mode'];
  if ($mode == 'test') {
    $payflowlink_url = 'https://pilot-payflowlink.paypal.com';
    $payflowpro_url = 'https://pilot-payflowpro.paypal.com';
  }
  else {
    $payflowlink_url = 'https://payflowlink.paypal.com';
    $payflowpro_url = 'https://payflowpro.paypal.com';
  }
  return array(
    'partner' => $settings['partner'],
    'merchant' => $settings['merchant'],
    'user' => $settings['user'],
    'password' => payflowpro_hcp_get_payflowpro_password($settings['key']),
    'mode' => $mode,
    'payflowlink_url' => $payflowlink_url,
    'payflowpro_url' => $payflowpro_url,
  );
}
