<?php

/**
 * @file
 * Settings form and related functions.
 */

/**
 * PayFlowPro Webform settings.
 */
function payflowpro_hcp_webform_settings_form(&$form, &$form_state, $form_id) {
  $nid = $form['nid']['#value'];
  $existing_values = payflowpro_hcp_webform_load_item($nid);
  // PayFlowPro settings form.
  $form['payflowpro'] = array(
    '#type' => 'fieldset',
    '#title' => 'PayFlowPro settings',
    '#collapsible' => 1,
    '#collapsed' => 1,
  );
  $form['payflowpro']['do_payment_process'] = array(
    '#type' => 'checkbox',
    '#title' => t('Do payment through PayFlowPro hosted checkout page after form submission'),
    '#default_value' => (isset($existing_values['do_payment_process']) ? $existing_values['do_payment_process'] : 0),
  );
  // Parameter configurations.
  $webform_fields = array('' => '');
  foreach ($form['webform']['#value']['components'] as $component) {
    if ($component['type'] != 'fieldset') {
      $webform_fields[$component['form_key']] = $component['name'];
    }
  }
  $form['payflowpro']['parameters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Parameters'),
    '#collapsible' => 1,
    '#collapsed' => 1,
  );
  $payflowpro_parameters = payflowpro_hcp_parameters();
  foreach ($payflowpro_parameters as $parameter => $param_obj) {
    $form['payflowpro']['parameters'][$parameter] = array(
      '#type' => 'select',
      '#title' => check_plain($parameter),
      '#options' => $webform_fields,
      '#default_value' => (isset($existing_values['parameters'][$parameter]) ? $existing_values['parameters'][$parameter] : ''),
      '#description' => check_plain((isset($param_obj['description']) ? $param_obj['description'] . ', ' : '') .
                        (isset($param_obj['type']) ? 'Type : ' . $param_obj['type'] : '') .
                        (isset($param_obj['length']) ? ', Length : ' . $param_obj['length'] : '') .
                        (isset($param_obj['required']) && $param_obj['required'] == TRUE ? ', Required : ' . 'TRUE' : '')),
    );
    if ($parameter == 'COMMENT1' || $parameter == 'COMMENT2') {
      $form['payflowpro']['parameters'][$parameter]['#type'] = 'textfield';
      unset($form['payflowpro']['parameters'][$parameter]['#options']);
    }
  }
  if (module_exists('token')) {
    $form['payflowpro']['parameters']['token_help'] = array(
      '#title' => t('Replacement patterns'),
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['payflowpro']['parameters']['token_help']['help']['#value'] = theme('token_help', 'PayFlowPro');
  }
  // Field to change after payment process.
  $form['payflowpro']['update_on_return'] = array(
    '#type' => 'fieldset',
    '#title' => t('Update Field after successful payment'),
    '#collapsible' => 1,
    '#collapsed' => 1,
  );
  $payflowpro_response_parameters = payflowpro_hcp_response_parameters();
  foreach ($payflowpro_response_parameters as $parameter => $param_obj) {
    $form['payflowpro']['update_on_return']['RET_' . $parameter] = array(
      '#type' => 'select',
      '#title' => check_plain($parameter),
      '#options' => $webform_fields,
      '#default_value' => (isset($existing_values['update_on_return']['RET_' . $parameter]) ? $existing_values['update_on_return']['RET_' . $parameter] : ''),
      '#description' => check_plain((isset($param_obj['description']) ? $param_obj['description'] . ', ' : '') .
                        (isset($param_obj['type']) ? 'Type : ' . $param_obj['type'] : '') .
                        (isset($param_obj['length']) ? ', Length : ' . $param_obj['length'] : '') .
                        (isset($param_obj['required']) && $param_obj['required'] == TRUE ? ', Required : ' . 'TRUE' : '')),
    );
  }
  // Redirect URL after payment process.
  $form['payflowpro']['redirect_to'] = array(
    '#type' => 'fieldset',
    '#title' => t('URL to redirect after payment process'),
    '#collapsible' => 1,
    '#collapsed' => 1,
  );
  $form['payflowpro']['redirect_to']['default_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Default Redirect URL'),
    '#default_value' => (isset($existing_values['redirect_urls']['default_url']) ? $existing_values['redirect_urls']['default_url'] : ''),
  );
  $form['payflowpro']['redirect_to']['success_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Redirect URL after success'),
    '#default_value' => (isset($existing_values['redirect_urls']['success_url']) ? $existing_values['redirect_urls']['success_url'] : ''),
  );
  $form['payflowpro']['redirect_to']['error_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Redirect URL after error transaction'),
    '#default_value' => (isset($existing_values['redirect_urls']['error_url']) ? $existing_values['redirect_urls']['error_url'] : ''),
  );
  $form['payflowpro']['redirect_to']['cancel_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Redirect URL on cancel activity'),
    '#default_value' => (isset($existing_values['redirect_urls']['cancel_url']) ? $existing_values['redirect_urls']['cancel_url'] : ''),
  );
}

/**
 * PayFlowPro Webform settings submissions.
 */
function payflowpro_hcp_webform_settings_form_submit($form, &$form_state) {
  $nid = $form_state['values']['nid'];
  $do_payment_process = $form_state['values']['do_payment_process'];
  $parameters = array();
  $payflowpro_parameters = payflowpro_hcp_parameters();
  foreach ($payflowpro_parameters as $parameter => $param_desc) {
    $parameters[$parameter] = $form_state['values'][$parameter];
  }
  $update_on_return = array();
  $payflowpro_response_parameters = payflowpro_hcp_response_parameters();
  foreach ($payflowpro_response_parameters as $parameter => $param_obj) {
    $update_on_return['RET_' . $parameter] = $form_state['values']['RET_' . $parameter];
  }
  $redirect_urls = array(
    'default_url' => $form_state['values']['default_url'],
    'success_url' => $form_state['values']['success_url'],
    'error_url' => $form_state['values']['error_url'],
    'cancel_url' => $form_state['values']['cancel_url'],
  );
  // Save data to table 'payflowpro_hcp_webform'.
  $existing_data = db_fetch_array(db_query('SELECT count(1) AS cnt FROM {payflowpro_hcp_webform} WHERE nid=%d', $nid));
  if ($existing_data['cnt'] == 0) {
    // Insert new data.
    db_query("INSERT INTO {payflowpro_hcp_webform}
                (nid, do_payment_process, parameters, update_on_return, redirect_urls)
              VALUES (%d, %d, '%s', '%s', '%s')",
              $nid, $do_payment_process, serialize($parameters), serialize($update_on_return), serialize($redirect_urls));
  }
  else {
    db_query("UPDATE {payflowpro_hcp_webform} SET
              do_payment_process = %d, parameters = '%s', update_on_return = '%s', redirect_urls = '%s'
              WHERE nid = %d",
              $do_payment_process, serialize($parameters), serialize($update_on_return), serialize($redirect_urls), $nid);
  }
}

/**
 * To load payflowpro_hcp_webform settings.
 */
function payflowpro_hcp_webform_load_item($nid) {
  $existing_values = array();
  // Get existing data.
  if (isset($nid) && !empty($nid)) {
    $result = db_query('SELECT do_payment_process, parameters, update_on_return, redirect_urls FROM {payflowpro_hcp_webform} WHERE nid=%d', $nid);
    if ($result_item = db_fetch_array($result)) {
      $existing_values['nid'] = $nid;
      $existing_values['do_payment_process'] = $result_item['do_payment_process'];
      $existing_values['parameters'] = unserialize($result_item['parameters']);
      $existing_values['update_on_return'] = unserialize($result_item['update_on_return']);
      $existing_values['redirect_urls'] = unserialize($result_item['redirect_urls']);
    }
  }
  return $existing_values;
}
