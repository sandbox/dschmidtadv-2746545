<?php

/**
 * @file
 * Menu callbacks and supporting functions.
 */

/**
 * Implements hook_menu().
 */
function payflowpro_hcp_menu_menu() {
  return array(
    'admin/settings/payflowpro_hcp' => array(
      'title' => 'PayFlowPro Account Settings',
      'description' => 'Settings for PayFlowPro account.',
      'page callback' => 'payflowpro_hcp_account_list',
      'access arguments' => array('configure PayFlowPro account'),
      'file' => 'payflowpro_hcp.admin.inc',
      'file path' => drupal_get_path('module', 'payflowpro_hcp') . '/includes',
    ),
    'admin/settings/payflowpro_hcp/add' => array(
      'title' => 'Add PayFlowPro Account',
      'description' => 'Configure PayFlowPro settings.',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('payflowpro_hcp_form'),
      'access arguments' => array('configure PayFlowPro account'),
      'file' => 'payflowpro_hcp.admin.inc',
      'file path' => drupal_get_path('module', 'payflowpro_hcp') . '/includes',
      'type' => 'MENU_NORMAL_ITEM',
    ),
    'admin/payflowpro_hcp/edit/%' => array(
      'title' => 'Edit PayFlowPro Account',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('payflowpro_hcp_edit_form'),
      'access arguments' => array('configure PayFlowPro account'),
      'file' => 'payflowpro_hcp.admin.inc',
      'file path' => drupal_get_path('module', 'payflowpro_hcp') . '/includes',
      'type' => 'MENU_NORMAL_ITEM',
    ),
    'admin/payflowpro_hcp/delete/%' => array(
      'title' => 'Edit PayFlowPro Account',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('payflowpro_hcp_delete_form'),
      'access arguments' => array('configure PayFlowPro account'),
      'file' => 'payflowpro_hcp.admin.inc',
      'file path' => drupal_get_path('module', 'payflowpro_hcp') . '/includes',
      'type' => 'MENU_NORMAL_ITEM',
    ),
    'payflowpro_hcp/return/%' => array(
      'title' => 'PayFlowPro On Hosted Checkout Page return',
      'description' => 'On PayFlowPro hosted checkout page return',
      'page callback' => 'payflowpro_hcp_complete',
      'page arguments' => array(2),
      'access arguments' => array('access content'),
      'file' => 'payflowpro_hcp.pay.inc',
      'file path' => drupal_get_path('module', 'payflowpro_hcp') . '/includes',
      'type' => 'MENU_CALLBACK',
    ),
  );
}
