<?php

/**
 * @file
 * Settings form and related functions.
 */

/**
 * PayFlowPro Webform submissions.
 */
function payflowpro_hcp_webform_form_submit($form, &$form_state) {
  $nid = $form_state['values']['details']['nid'];
  $sid = $form_state['values']['details']['sid'];
  $order_id = "wf-$nid-$sid";
  // Set parameter values.
  $parameters = array();
  $parameters['ORDERID'] = $order_id;
  require_once 'payflowpro_hcp_webform.admin.inc';
  $payflowpro_webform = payflowpro_hcp_webform_load_item($nid);
  $components = payflowpro_hcp_webform_component_mapping($nid);
  $component_values = array();

  require_once drupal_get_path('module', 'webform') . '/includes/webform.submissions.inc';
  $submission = webform_get_submission($nid, $sid);
  foreach ($components as $field_name => $cid) {
    if (isset($submission->data[$cid]['value'][0])) {
      if (!isset($submission->data[$cid]['value'][0])) {
        $submission->data[$cid]['value'][0] = $submission->data[$cid]['value'][0];
      }
      $value = $submission->data[$cid]['value'][0];

      if (is_array($value)) {
        $value = implode(',', $value);
      }
      $component_values[$field_name] = $value;
    }
  }
  foreach ($payflowpro_webform['parameters'] as $key => $field_name) {
    if (!empty($field_name)) {
      if (isset($component_values[$field_name])) {
        $parameters[$key] = $component_values[$field_name];
      }
      else {
        $parameters[$key] = token_replace($field_name, 'Submission', $component_values);
      }
    }
  }
  $parameters['TRXTYPE'] = 'S';

  // Invoke hook_payflowpro_hcp_webform_prepayment.
  foreach (module_implements('payflowpro_hcp_webform_prepayment') as $module) {
    $function = $module . '_payflowpro_hcp_webform_prepayment';
    $function($parameters, $form_state);
  }
  // Initiate payment process.
  $payment_response = payflowpro_hcp_invoke_payment($parameters);
  if ($payment_response != '') {
    $form_state['rebuild'] = TRUE;
    $form_state['error'] = TRUE;
    drupal_goto('payment/error');
    return;
  }
}

/**
 * This function executed after completing payment process.
 */
function payflowpro_hcp_webform_on_complete($action, $response) {
  $order_splitted = explode('-', $response['ORDERID']);
  $nid = $order_splitted[1];
  $sid = $order_splitted[2];
  $components = payflowpro_hcp_webform_component_mapping($nid);
  require_once 'payflowpro_hcp_webform.admin.inc';
  $webform_configs = payflowpro_hcp_webform_load_item($nid);
  $node = node_load($nid);
  $submission = webform_menu_submission_load($sid, $nid);
  if (isset($submission->sid)) {
    foreach ($webform_configs['update_on_return'] as $parameter => $field_name) {
      if ($field_name != '') {
        if (isset($components[$field_name])) {
          $cid = $components[$field_name];
          $parameter_key = str_replace('RET_', '', $parameter);
          if (isset($response[$parameter_key])) {
            $value = $response[$parameter_key];
            $submission->data[$cid]['value'][0] = $value;
          }
        }
      }
    }
    webform_submission_update($node, $submission);
  }

  // Invoke hook  hook_webform_payflowpro_hcp_complete.
  module_invoke_all('webform_payflowpro_hcp_post_complete', $action, $response);

  // No need to redirect for silent post.
  if ($action == 'silent_post') {
    echo 'Data saved to registration';
    exit;
  }

  // Load values in session so that it can be used in next page.
  $_SESSION['payflowpro_hcp_response'] = $response;
  // Redirect to configured page.
  if (isset($webform_configs['redirect_urls'][$action . '_url']) && $webform_configs['redirect_urls'][$action . '_url'] != '') {
    drupal_goto($webform_configs['redirect_urls'][$action . '_url']);
  }
  elseif (isset($webform_configs['redirect_urls']['default_url']) && $webform_configs['redirect_urls']['default_url'] != '') {
    drupal_goto($webform_configs['redirect_urls']['default_url']);
  }
}

/**
 * Function returns array with webform field key and cid.
 */
function payflowpro_hcp_webform_component_mapping($nid) {
  $node = node_load($nid);
  $components = array();
  if (isset($node->webform['components'])) {
    foreach ($node->webform['components'] as $component) {
      $field_key = $component['form_key'];
      $field_cid = $component['cid'];
      $components[$field_key] = $field_cid;
    }
  }
  return $components;
}
