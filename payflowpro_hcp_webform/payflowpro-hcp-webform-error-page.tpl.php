<?php
/**
 * @file
 * Template to display.
 *
 * @template payflowpro-hcp-webform-error-page
 * @theme_name payflowpro_hcp_webform_error_page
 */
?>
<div class="messages error">
  Payment system is currently unavailable. Please contact site administrator
</div>
