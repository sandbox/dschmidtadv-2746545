  Description
====================================
This module implements PayPal payment service "PayFlowPro Hosted Checkout Pages"
. PayFlowPro Hosted Checkout Page solves PCI compliance in payment service. As
of now there is no contributed module that implements Hosted Checkout Pages in
PayFlowPro.

This project contains 2 modules payflowpro_hcp and payflowpro_hcp_webform, 
download the modules and enable same in website.

payflowpro_hcp :
1) Create custom page to display message for Payment Cancellation
2) Create PayFlowPro account and configure Hosted Checkout Page
  - Go to URL https://manager.paypal.com/
  - Click the link "I would like to create a new account" below Login button
  - Provide necessary details and create a account
  - Configure Hosted Checkout Page in account
  - Remenber to configure Success, Error and Silent Post URL as below
    http://<website base URL>/payflowpro_hcp/return/success
    http://<website base URL>/payflowpro_hcp/return/error
    http://<website base URL>/payflowpro_hcp/return/silent_post
3) Configure PayPal account in drupal website
  - User should configure PayFlowPro Hosted Checkout Page account in settings 
  page /admin/settings/payflowpro_hcp
  - Click link "Add New PayFlowPro Account" in page to add new PayPal account.
  - Configure any one account as default account

payflowpro_hcp_webform :
1) Create a webform

2) Add fields to webform Amount, etc.,

3) Edit webform settings
3.1) Navigate down to "PayFlowPro Settings"
3.2) Turn on "Do payment through PayFlowPro hosted checkout page after form 
submission" checkbox, that enabled PayFlowPro for this webform
3.3) Configure "Parameters" (PayFlowPro Input Parameters), need to choose 
webform fields from which values to be taken for each PayFlowPro parameters
3.3.1) The parameter Amount is mandatory, choose webform field from which amount 
can be taken for PayFlowPro input
3.3.2) Choose appropriate fields for other parameters
3.4) Configure "Update Field after successful payment", configure fields to 
which PayFlowPro response to be updated in submission after payment process
3.4.1) Configure HiddenFields for necessary parameters, after completing payment
process the values from response will be stored back to this fields
3.5) Configure "URL to redirect after payment process"
3.5.1) Create custom thank you page, create page using content type "Page",
check "Replacement Tokens" that can be used in thank you page, tokens contains
PayFlowPro response and form submission [submission-%]
3.5.2) Configure page URL in settings "URL to redirect after payment process",
after completing payment process the page will redirect to configured Thank You
page
3.6) Configure emails for webform

4) Unit testing
4.1) On submitting form with Amount, the page will store data to drupal and 
redirect page to PayFlowPro Hosted Checkout Page
4.2) Do payment process
4.3) After completing payment process, page will update PayFlowPro responses 
to the configured fields, trigger emails configured in Email settings and 
redirects to Thank You page
